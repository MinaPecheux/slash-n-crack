using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    [SerializeField] private RectTransform _logoShadow;
    [SerializeField] private RectTransform _logo;
    [SerializeField] private RectTransform _titleShadow;
    [SerializeField] private RectTransform _title;

    [SerializeField] private Button _btnTestCrash;
    [SerializeField] private Text _toolsInfoText;
    [SerializeField] private Text _toolsCrashlyticsInfoText;

    private void Awake()
    {
        _btnTestCrash.onClick.AddListener(
            CrashlyticsInitializer.instance.TestCrash);
        _toolsInfoText.text = $"version: {Application.version}";
    }

    private void Start()
    {
        StartCoroutine(UIAnimations.ScaleUIElement(
            _logoShadow,
            duration: 2,
            wait: 0.2f,
            easing: UIAnimations.AnimationEasing.BounceOut));
        StartCoroutine(UIAnimations.ScaleUIElement(
            _logo,
            duration: 2,
            wait: 0.25f,
            easing: UIAnimations.AnimationEasing.BounceOut));
        StartCoroutine(UIAnimations.ScaleUIElement(
            _titleShadow,
            duration: 2,
            wait: 0.4f,
            easing: UIAnimations.AnimationEasing.BounceOut));
        StartCoroutine(UIAnimations.ScaleUIElement(
            _title,
            duration: 2,
            wait: 0.45f,
            easing: UIAnimations.AnimationEasing.BounceOut));
    }

    public void CheckCrashlytics()
    {
        if (CrashlyticsInitializer.instance.isInitialized)
            _toolsCrashlyticsInfoText.text = $"<color=green>crashlytics: ok</color>";
        else
            _toolsCrashlyticsInfoText.text = $"<color=red>crashlytics: ko</color>";
    }

    public void Play()
    {
        SceneBooter.instance.LoadGame();
    }

    public void QuitGame()
    {
        SceneBooter.instance.QuitGame();
    }
}
